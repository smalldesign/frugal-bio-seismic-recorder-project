'''Python script to record optical mouse movements
	For the Hathi-March BioSeismic project (Frugal Science)
	Team - 
		- Sumithra Suredralal
		- Nish Kothari
		- Cecilia Herbert
		- Subir Bhaduri
	
	Features:
		- records Mouse XY movements with timestamps.
		- Has a set sampling frequency
		- Has a DC bias removing piece, because mouse tend to have large timely drifts.
		- Records files of 1 hour durations for easy transfer and post processing.
		- Can be run continiously or quit by 'ctrl+c'.
		- Runs on Raspberry Pi or Linux shells (without GUI) as well as Windows DOS
		- Shows a graphical representation on screen.
	
	updated - 9th Nov. 2020, Subir
	
	updated 15h Nov 2020 (Subir)
		- Changed from logger to CSV writer to minimize the load of writing to disc every 1ms or so. 
	
	updated 16th Nov. 2020 (subir)
		- Record raw data. 
		- Do not remove drift
		- Do not sample uniformly.
		- All processes can be done in post processing.
		
	updated 17th Nov 2020.
		- Added a live plotter to better show the effect.
	
	updated 24th Nov 2020.
		- Update for linux and windows.
		- add code to quit program on keyboard interrupt.
	
	updated ? - 21Dec2020
		- checked plotter.
'''

#-------------------------------------------------------------------------------
# Time keeping libraries.
import time
from datetime import datetime, timedelta
import numpy as np

# import os library for platform specific file storage stuff.
import os
# import platform to determine if wondows or linux/Mac.
import platform

# For logging data with local timestamp
#import logging, logging.handlers

# Importing csv library for storing data in csv format. 
import csv 



# plotting related
#-------------------------------------------------------------------------------
import sys
import pyqtgraph as pg
from collections import deque
from pyqtgraph.Qt import QtGui, QtCore

# for quitting pyqt application
# ref: https://stackoverflow.com/questions/5160577/ctrl-c-doesnt-work-with-pyqt
import signal
#signal.signal(signal.SIGINT, signal.SIG_DFL)

#signal.signal(signal.CTRL_C_EVENT, signal.SIG_DFL)

''' Identify the mouse
-------------------------------------------------------------------------------
'''

# Mouse python object 
#-------------------------------------------------------------------------------
mouse= None

# Find the mouse!
#-------------------------------------------------------------------------------
# For windows systems
# ref: https://pythonhosted.org/pynput/mouse.html
if platform.system() == 'Windows':
	OS = 'Windows'
	#from pynput.mouse import Listener
	from pynput.mouse import Button, Controller
	mouse = Controller()
	
# For linux systems
# ref: https://gist.github.com/tstenner/8c70d6d0068e00b30012cb6ac97ebeb9
elif platform.system() == 'Linux':
	OS='Linux'
	import evdev
	import evdev.ecodes as ev
	for fn in evdev.list_devices():
		dev = evdev.InputDevice(fn)
		cap = dev.capabilities()
		if ev.EV_KEY in cap:
			if ev.BTN_MOUSE in cap[ev.EV_KEY]:
				mouse = dev
				break

# Confirm if mouse has been found.
if mouse:
	print('Started listening to mouse:')
else: print('Mouse not found !')


	
''' Read mouse coordinates and plot data and record
-------------------------------------------------------------------------------
'''
class mouse_logger_plotter:
	def __init__(self ):
		self.dat = deque()
		self.maxLen = 1000 					#max number of data points to show on graph
		self.app = QtGui.QApplication([])
		self.win = pg.GraphicsWindow()
		
		# How much to average over to remove DC bias?
		self.MA_window_length = 20		# MA= Moving Average window lenght
		self.index = 0					# Index for the above 
		
		# X and Y arrays.
		self.Ylist = [0]*self.MA_window_length	# List of last  few X values
		self.Xlist = [0]*self.MA_window_length	# List of last  few X values
		
		# Initiate the X and Y points.
		self.X=0
		self.Y=0
		
		# logging related.
		self.log_after_lines = 100			# record value to a file after thesemany lines.
		self.record=[]						# varaible to hold data before recording.
		self.filename=''
		self.new_file()
		
		# Plotting 
		self.p1 = self.win.addPlot()		# add a plot to the screen.
		self.curve1 = self.p1.plot()		# a curve that will be updated often.
	   
		# Initate the timer
		graphUpdateSpeedMs = 10
		timer = QtCore.QTimer()				# to create a thread that calls a function at intervals
		timer.timeout.connect(self.update)	# the update function keeps getting called at intervals
		timer.start(graphUpdateSpeedMs)	    # start the timer
		
		#self.quit=False						# something to indicate that the program must not be quit.
		
		#sys.exit(QtGui.QApplication.instance().exec_())	
		sys.exit(self.app.exec_())
	
	# Create a new logging file.
	#-------------------------------------------------------------------------------
	def new_file(self):
		self.file_creation_time = datetime.now()		
		# Convert this pythony gibberish datetime object into a human readable data and time string.
		dt_string = self.file_creation_time.strftime("%d%m%Y_%H-%M-%S")
		# Filename of a new csv file to store data in 
		self.filename = 'data_' + dt_string + '.csv' 
		
	# Get mouse coordinates for linux/Raspberry Pi
	#-------------------------------------------------------------------------------
	def Linux_update_mouse_XY(self):
		event = mouse.read_one()							# read a mouse event, the last one.
		if event != None:									# Check if its a 'good' event.
			if event.type == evdev.ecodes.EV_REL:			# If its a relevative movemetn
				if event.code == evdev.ecodes.REL_X:		# in X direction
					self.X += event.value						# Increment to get absolute values.
				if event.code == evdev.ecodes.REL_Y:		# or in Y
					self.Y += event.value						# Increment to get absolute values.
	
	# Actual update script of X and Y as per timer, and plotter, and logger.
	#-------------------------------------------------------------------------------
	def update(self):		
		#global X
		if len(self.dat) > self.maxLen:
			self.dat.popleft() #remove oldest
		
		# read mouse live
		if OS =='Windows':
			self.X,self.Y = mouse.position
		else:
			Linux_update_mouse_XY()
		
		self.Ylist[self.index] = self.Y
		self.Xlist[self.index] = self.X
		self.index = self.index + 1
		
		if self.index == self.MA_window_length: self.index=0
		self.X = self.X - int(np.mean(self.Xlist))				# Remove moving average, to remove DC bias.
		self.Y = self.Y - int(np.mean(self.Ylist))				# Remove moving average, to remove DC bias.
		
		#print(X)
		self.dat.append(self.Y) 
		self.record.append([datetime.now(),self.X,self.Y])	
	
		# If records pass so many lines mark, dump the records list onto a file, empty the records and start again.
		if len(self.record) > self.log_after_lines:
			# writing the data into the file 
			with open(self.filename, 'a') as f :	 
				write = csv.writer(f) 
				write.writerows(self.record) 
				self.record=[]
			
			if datetime.now()- self.file_creation_time > timedelta(hours = 1):
				self.new_file()
		
		# Put data on plotting curve
		self.curve1.setData(self.dat)
		# plot.
		self.app.processEvents() 
	   

''' Launch application.
-------------------------------------------------------------------------------
'''
if __name__ == '__main__':
	app=QtGui.QApplication(sys.argv)
	g = mouse_logger_plotter()
	# start the app 
	sys.exit(app.exec()) 
	
#XXXXXXXXXXXX
# End of file.
#XXXXXXXXXXXX
